﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;   // see on vajalik, kui ma tahan andmetega toimetada

namespace MinuEsimeneDB
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = "Data Source=valiitsql.database.windows.net;Initial Catalog=Northwind;Persist Security Info=True;User ID=student;Password=Pa$$w0rd";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            SqlCommand comm = new SqlCommand("select productname, unitprice from products", conn);

            var R = comm.ExecuteReader();

            while (R.Read())
            {
                Console.WriteLine(R["productname"]);
            }
            conn.Close();
        }
    }
}
