﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinuKolmasDB
{
    class Program
    {
        static void Main(string[] args)
        {
            // alustan sellest, et teen Entity objekti
            // selle käest saab siis "asju küsida"
            NorthwindEntities db = new NorthwindEntities();

            // db.Database.Log = Console.WriteLine;  // huvi pärast


            foreach(var p in db.Products
                .Where(x => x.UnitPrice < 10)
                .OrderBy(x => x.ProductName)
                .Select(x => new {x.ProductName, x.UnitPrice, Laoseis = x.UnitPrice * x.UnitsInStock})
                )
                Console.WriteLine(p);
            // nii saan andmebaasi mööda uurida ja asju kätte

            // kaks erinevat viisi, kuidas kirjutada C# keeles
            var q1 = db.Products
                    .Where(x => x.UnitPrice < 10)
                    .OrderBy(x => x.UnitPrice)
                    .Select(x => new { x.ProductName, x.UnitPrice })
                    ;
            var q2 = from x in db.Products
                     where x.UnitPrice < 10
                     orderby x.UnitPrice
                     select new { x.ProductName, x.UnitPrice }
                     ;
            // Q1 ja Q2 on identsed "kollektsioonid"
            foreach (var x in q1) Console.WriteLine(x); // q1 asemele q2 - vahet pole

            // üks näide, kuidas andmebaasi sisu muuta (8 on selle kategooria võti-key)
            db.Categories.Find(8).CategoryName = "Seafood"; // muudatus mälus
            db.SaveChanges(); // salvestame baasi 
            


            


        }
    }
}
