﻿namespace MinuKolmasAken
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.LisamisNupp = new System.Windows.Forms.Button();
            this.NimeBox = new System.Windows.Forms.TextBox();
            this.VanuseBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.VeaLabel = new System.Windows.Forms.Label();
            this.KustutusNupp = new System.Windows.Forms.Button();
            this.SalvestaNupp = new System.Windows.Forms.Button();
            this.SaveJson = new System.Windows.Forms.Button();
            this.LoeJson = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(43, 26);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Tere";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(164, 28);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 2;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(46, 87);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Näita inimesi";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(46, 147);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(344, 150);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.TabStop = false;
            // 
            // LisamisNupp
            // 
            this.LisamisNupp.Location = new System.Drawing.Point(315, 86);
            this.LisamisNupp.Name = "LisamisNupp";
            this.LisamisNupp.Size = new System.Drawing.Size(75, 23);
            this.LisamisNupp.TabIndex = 5;
            this.LisamisNupp.Text = "Lisa";
            this.LisamisNupp.UseVisualStyleBackColor = true;
            this.LisamisNupp.Click += new System.EventHandler(this.LisamisNupp_Click);
            // 
            // NimeBox
            // 
            this.NimeBox.Location = new System.Drawing.Point(400, 100);
            this.NimeBox.Name = "NimeBox";
            this.NimeBox.Size = new System.Drawing.Size(150, 20);
            this.NimeBox.TabIndex = 6;
            // 
            // VanuseBox
            // 
            this.VanuseBox.Location = new System.Drawing.Point(400, 126);
            this.VanuseBox.Name = "VanuseBox";
            this.VanuseBox.Size = new System.Drawing.Size(150, 20);
            this.VanuseBox.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(569, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Siia kasti käib nimi";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(569, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Siia kasti käib vanus";
            // 
            // VeaLabel
            // 
            this.VeaLabel.AutoSize = true;
            this.VeaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.VeaLabel.ForeColor = System.Drawing.Color.Red;
            this.VeaLabel.Location = new System.Drawing.Point(396, 149);
            this.VeaLabel.Name = "VeaLabel";
            this.VeaLabel.Size = new System.Drawing.Size(0, 20);
            this.VeaLabel.TabIndex = 10;
            // 
            // KustutusNupp
            // 
            this.KustutusNupp.Location = new System.Drawing.Point(164, 85);
            this.KustutusNupp.Name = "KustutusNupp";
            this.KustutusNupp.Size = new System.Drawing.Size(75, 23);
            this.KustutusNupp.TabIndex = 11;
            this.KustutusNupp.Text = "Kustuta";
            this.KustutusNupp.UseVisualStyleBackColor = true;
            this.KustutusNupp.Click += new System.EventHandler(this.KustutusNupp_Click);
            // 
            // SalvestaNupp
            // 
            this.SalvestaNupp.Location = new System.Drawing.Point(46, 325);
            this.SalvestaNupp.Name = "SalvestaNupp";
            this.SalvestaNupp.Size = new System.Drawing.Size(75, 23);
            this.SalvestaNupp.TabIndex = 12;
            this.SalvestaNupp.Text = "Salvesta";
            this.SalvestaNupp.UseVisualStyleBackColor = true;
            this.SalvestaNupp.Click += new System.EventHandler(this.SalvestaNupp_Click);
            // 
            // JsonNupp
            // 
            this.SaveJson.Location = new System.Drawing.Point(46, 367);
            this.SaveJson.Name = "JsonNupp";
            this.SaveJson.Size = new System.Drawing.Size(75, 23);
            this.SaveJson.TabIndex = 13;
            this.SaveJson.Text = "Save Json";
            this.SaveJson.UseVisualStyleBackColor = true;
            this.SaveJson.Click += new System.EventHandler(this.SaveJson_Click);
            // 
            // LoeJson
            // 
            this.LoeJson.Location = new System.Drawing.Point(143, 366);
            this.LoeJson.Name = "LoeJson";
            this.LoeJson.Size = new System.Drawing.Size(75, 23);
            this.LoeJson.TabIndex = 14;
            this.LoeJson.Text = "Loe Json";
            this.LoeJson.UseVisualStyleBackColor = true;
            this.LoeJson.Click += new System.EventHandler(this.LoeJson_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.LoeJson);
            this.Controls.Add(this.SaveJson);
            this.Controls.Add(this.SalvestaNupp);
            this.Controls.Add(this.KustutusNupp);
            this.Controls.Add(this.VeaLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.VanuseBox);
            this.Controls.Add(this.NimeBox);
            this.Controls.Add(this.LisamisNupp);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button LisamisNupp;
        private System.Windows.Forms.TextBox NimeBox;
        private System.Windows.Forms.TextBox VanuseBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label VeaLabel;
        private System.Windows.Forms.Button KustutusNupp;
        private System.Windows.Forms.Button SalvestaNupp;
        private System.Windows.Forms.Button SaveJson;
        private System.Windows.Forms.Button LoeJson;
    }
}

