﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinuKolmasAken
{
    public class Inimene
    {
        public int KingaNumber { get; set; }
        public string Nimi { get; set; }
        public int Vanus { get; set; }
        public decimal Palk { get; set; }
    }
}
