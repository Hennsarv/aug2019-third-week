﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;

namespace MinuKolmasAken
{
    public partial class Form1 : Form
    {
        static readonly string filename = @"..\..\inimesed.txt";

        List<Inimene> Inimesed = new List<Inimene>
        {
            new Inimene {Nimi = "Henn", Vanus = 64}
        };


        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = Inimesed;
        }

        private void LisamisNupp_Click(object sender, EventArgs e)
        {
            // kontrollime enne, kas on hää
            // kas  nimi on olemas
            // kas vanus on ikka arv
            // kas vanus on mõistlik arv
            VeaLabel.Text = "";
            string nimi = NimeBox.Text;

            if (nimi.Length < 2) VeaLabel.Text += "nimi liiga lühike\n"; 
            int vanus;
            if (! int.TryParse(VanuseBox.Text, out vanus))  VeaLabel.Text += "vanus vigane\n";
            if (vanus < 18) VeaLabel.Text += "liiga noor\n";
            if (vanus >120) VeaLabel.Text += "liiga vana\n";

            if (VeaLabel.Text != "") return;
            try
            {
                Inimene uus = new Inimene
                {
                    Nimi = nimi,
                    Vanus = vanus
                };
                Inimesed.Add(uus);
                NimeBox.Text = "";
                VanuseBox.Text = "";
                VeaLabel.Text = "";
            }
            catch (Exception err)
            {
                VeaLabel.Text = err.Message;
            }
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Inimesed;
        }

        private void KustutusNupp_Click(object sender, EventArgs e)
        {
            VeaLabel.Text = "";  // silumise mõttes kasutan veateate välja!
            if (dataGridView1.SelectedRows.Count != 1)
            {
                VeaLabel.Text = "Kustuta ühe kaupa\nklõpsa rea päisel";
                return;
            }

            foreach (var x in this.dataGridView1.SelectedRows)
            {
                var nr = ((DataGridViewRow)x).Index;
                //VeaLabel.Text = "Kustutan inimese " + Inimesed[nr].Nimi + "\n";
                Inimesed.RemoveAt(nr);
            }
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Inimesed;
        }

        private void SalvestaNupp_Click(object sender, EventArgs e)
        {
            File.WriteAllLines(filename,
                Inimesed.Select( x => $"{x.Nimi},{x.Vanus}")
                );
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // see on meetod, mis pannakse akna laadimisel käima
            try
            {
                Inimesed =
                       File.ReadAllLines(filename)
                       .Select(x => x.Split(','))
                       .Select(x => new Inimene { Nimi = x[0], Vanus = int.Parse(x[1]) })
                       .ToList();
            }
            catch (Exception)
            {
                // seda ma teen siis, kui fali ei ole või ta on vigane
                Inimesed = new List<Inimene>
                {
                    new Inimene {Nimi = "Adam", Vanus = 4000}
                };
            }
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Inimesed;
        }

        private void SaveJson_Click(object sender, EventArgs e)
        {
            string jsonFilename = @"..\..\inimesed.json";
            string json = JsonConvert.SerializeObject(Inimesed);
            File.WriteAllText(jsonFilename, json);
        }

        private void LoeJson_Click(object sender, EventArgs e)
        {
            string jsonFilename = @"..\..\inimesed.json";
            Inimesed = 
                JsonConvert.DeserializeObject<List<Inimene>>(
                    File.ReadAllText(jsonFilename)
                    );
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Inimesed;

        }
    }
}
