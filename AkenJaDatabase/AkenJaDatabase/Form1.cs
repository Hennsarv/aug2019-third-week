﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AkenJaDatabase
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string categoryName = ((ListBox)sender).SelectedItem.ToString();
            NorthwindEntities db = new NorthwindEntities();

            this.dataGridView1.DataSource = 
            db.Categories.Where(x => x.CategoryName == categoryName)
                .Single().Products.Select(x => new { x.ProductID, x.ProductName, x.UnitPrice })
                .ToList();

            // need read me lisasime töö käiguis, et oleks ilusam
            dataGridView1.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView1.Columns[0].HeaderText = "Tootekood";
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red; // see millegi pärast ei toimi



        }

        private void Form1_Load(object sender, EventArgs e)
        {
            NorthwindEntities db = new NorthwindEntities();
            this.listBox1.DataSource = db.Categories.Select(x => x.CategoryName).ToList();
        }
    }
}
