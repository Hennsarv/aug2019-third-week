﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EsimeneMVC.Models
{
    public class Inimene
    {
        static int nr = 0;

        public static List<Inimene> Inimesed = new List<Inimene>()
        {
            new Inimene {Nimi = "Henn"},
            new Inimene {Nimi = "Ants"},
            new Inimene {Nimi = "Peeter"},
        };

        [Display(Name ="PersonId")]public int Nr { get; } = ++nr;
        [Display(Name = "Full name")]public string Nimi { get; set; }

        public static Inimene Get(int id)
        {
            return Inimesed.Where(x => x.Nr == id).SingleOrDefault();
        }
    }


}