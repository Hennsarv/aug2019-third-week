﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EsimeneMVC.Models;

namespace EsimeneMVC.Controllers
{
    public class InimesedController : Controller
    {
        // GET: Inimesed
        public ActionResult Index()
        {
            return View(Inimene.Inimesed);
        }

        // GET: Inimesed/Details/5
        public ActionResult Details(int id)
        {
            return View(Inimene.Get(id));
        }

        // GET: Inimesed/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inimesed/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                Inimene.Inimesed.Add(
                    new Inimene { Nimi = collection["Nimi"]}
                    );

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimesed/Edit/5
        public ActionResult Edit(int id)
        {
            return View(Inimene.Get(id));
        }

        // POST: Inimesed/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                Inimene i = Inimene.Get(id);
                if(i != null)
                {
                    i.Nimi = collection["Nimi"];
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimesed/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Inimesed/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
