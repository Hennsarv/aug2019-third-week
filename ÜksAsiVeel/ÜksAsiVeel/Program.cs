﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ÜksAsiVeel
{
    class Program
    {
        static void Main(string[] args)
        {
            var henn = new Inimene { Nimi = "Henn", Vanus = 64 };
            var malle = new Inimene { Nimi = "Malle", Vanus = 32 };

            henn.Pidu += x => Console.WriteLine($"{x.Nimi} peab {x.Vanus} sünnipäeva pidu");
            malle.Pension += x => Console.WriteLine($"{x.Nimi} sul on aeg pensionile minna");
            malle.Pension += x => Console.WriteLine($"{x.Nimi} tee töökoht vabaks");


            Console.WriteLine(henn);
            for (; henn.Vanus++ < 100;) ;
            for (; malle.Vanus++ < 100;) ;
            Console.WriteLine(henn);
        }
    }

    class Inimene
    {
        public event Action<Inimene> Pidu;
        public event Action<Inimene> Pension;
        private int _Vanus;
        public string Nimi { get; set; }
        public int Vanus
        {
            get => _Vanus;
            set
            {
                _Vanus = value;
                if (_Vanus % 25 == 0)
                    // if (Pidu != null) Pidu(this);
                    Pidu?.Invoke(this);
                if (_Vanus > 75) Pension?.Invoke(this);

            }
        }

        public override string ToString() => $"{Nimi} on {Vanus} aastane";
    }
}
