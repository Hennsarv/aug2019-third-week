﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeineMVC.Models;

namespace TeineMVC.Controllers
{
    public class HomeController : Controller
    {
        public string Lihtne()
        {
            return "Hello world!";
        }

        public string Tere(string id)
        {
            return $"Tere {id}!";
        }

        public ActionResult Nimekiri()
        {
            return View(Inimene.Inimesed);
        }


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}