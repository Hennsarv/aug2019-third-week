﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TeineMVC;

namespace TeineMVC.Controllers
{
    public class SubjectTeachersController : Controller
    {
        private KaruAabitsEntities db = new KaruAabitsEntities();

        // GET: SubjectTeachers
        public ActionResult Index()
        {
            var subjectTeacher = db.SubjectTeacher.Include(s => s.Person).Include(s => s.Subject);
            return View(subjectTeacher.ToList());
        }

        // GET: SubjectTeachers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubjectTeacher subjectTeacher = db.SubjectTeacher.Find(id);
            if (subjectTeacher == null)
            {
                return HttpNotFound();
            }
            return View(subjectTeacher);
        }

        // GET: SubjectTeachers/Create
        public ActionResult Create()
        {
            ViewBag.PersonId = new SelectList(db.Person.Where(x => x.IsTeacher), "Id", "FullName");
            ViewBag.SubjectId = new SelectList(db.Subject, "Id", "Name");
            return View();
        }

        // POST: SubjectTeachers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PersonId,SubjectId")] SubjectTeacher subjectTeacher)
        {
            if (ModelState.IsValid)
            {
                db.SubjectTeacher.Add(subjectTeacher);
                try
                {
                    db.SaveChanges();
                }
                catch {}
                return RedirectToAction("Index");
            }

            ViewBag.PersonId = new SelectList(db.Person.Where(x => x.IsTeacher), "Id", "FullName", subjectTeacher.PersonId);
            ViewBag.SubjectId = new SelectList(db.Subject, "Id", "Name", subjectTeacher.SubjectId);
            return View(subjectTeacher);
        }

        // GET: SubjectTeachers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubjectTeacher subjectTeacher = db.SubjectTeacher.Find(id);
            if (subjectTeacher == null)
            {
                return HttpNotFound();
            }
            ViewBag.PersonId = new SelectList(db.Person.Where(x => x.IsTeacher), "Id", "FullName", subjectTeacher.PersonId);
            ViewBag.SubjectId = new SelectList(db.Subject, "Id", "Name", subjectTeacher.SubjectId);
            return View(subjectTeacher);
        }

        // POST: SubjectTeachers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PersonId,SubjectId")] SubjectTeacher subjectTeacher)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subjectTeacher).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                }
                catch { }
                return RedirectToAction("Index");
            }
            ViewBag.PersonId = new SelectList(db.Person.Where(x => x.IsTeacher), "Id", "FullName", subjectTeacher.PersonId);
            ViewBag.SubjectId = new SelectList(db.Subject, "Id", "Name", subjectTeacher.SubjectId);
            return View(subjectTeacher);
        }

        // GET: SubjectTeachers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubjectTeacher subjectTeacher = db.SubjectTeacher.Find(id);
            if (subjectTeacher == null)
            {
                return HttpNotFound();
            }
            return View(subjectTeacher);
        }

        // POST: SubjectTeachers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SubjectTeacher subjectTeacher = db.SubjectTeacher.Find(id);
            db.SubjectTeacher.Remove(subjectTeacher);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
