﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TeineMVC.Models
{
    public class Inimene
    {
        static Dictionary<int, Inimene> _Inimesed = new Dictionary<int, Inimene>()
                {
            {1,  new Inimene {Nr = 1, Eesnimi = "Henn", Perenimi = "Sarv"} },
            {2,  new Inimene {Nr = 2, Eesnimi = "Ants", Perenimi = "Saunamees"} },
            {3,  new Inimene {Nr = 3, Eesnimi = "Peeter", Perenimi = "Suur"} },
        };



        public static IEnumerable<Inimene> Inimesed => _Inimesed.Values;


        [Key]
        public int Nr { get; set; }
        public string Eesnimi { get; set; }
        public string Perenimi { get; set; }

        // klasssis võiks olla staatiline meetod, mis annaks võtme järgi leida inimesi
        public static Inimene Get(int nr) => 
            _Inimesed.ContainsKey(nr) ? _Inimesed[nr] : null;

        // vahel on viisakas teha meetod uue inimese lisamiseks
        public static void Add(Inimene inimene)
        {
            if (!_Inimesed.ContainsKey(inimene.Nr)) _Inimesed.Add(inimene.Nr, inimene);
        }

        // ja vajadusel inimese kustutamiseks
        public void Remove() => _Inimesed.Remove(this.Nr);

    }
}