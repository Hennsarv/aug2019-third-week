﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MinuTeineAken
{
    public partial class Form1 : Form
    {
        List<Inimene> inimesed = new List<Inimene>
            {
                new Inimene{Nimi = "Henn", Vanus = 64 },
                new Inimene{Nimi = "Ants", Vanus = 32 },
                new Inimene{Nimi = "Peeter", Vanus = 40 },
            };

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            this.dataGridView1.DataSource = inimesed;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            inimesed.Add(
                new Inimene
                {
                    Nimi = this.textBox1.Text,
                    Vanus = int.Parse(this.textBox3.Text),
                    Kinganumber = int.Parse(this.textBox2.Text)
                }

                );
            this.dataGridView1.DataSource = null;
            this.dataGridView1.DataSource = inimesed;


        }
    }
}
