﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinuKolmasDBKoos
{
    partial class Employee
    {
        public string FullName => FirstName + " " + LastName;
    }

    class Program
    {
        static void Main(string[] args)
        {

            int? arv = 7;
            arv = 0; // lubatud
            arv = null; // ei saa teha
            Nullable<int> teine = null;

            Console.WriteLine(   arv ?? -1   );
            Console.WriteLine( arv.HasValue ? arv.Value : -1 );



            NorthwindEntities db = new NorthwindEntities();

            foreach (var x in db.Employees)
                Console.WriteLine($"{x.FullName} ülemus on {(x.Manager?.FullName??"jumal")}");

            // ? - asjad

            // int?   - nullable andmetüüp - Nullable<int>
            // avaldis?.meetod()  -- tinglik väljakutse
            // avaldis?.property   
            // avaldis ?? asendusväärtus -- nulli asemel midgai muud


            


        }
    }
}
