﻿using System;

namespace Henn
{

    class Program
    {
        

        static void Main(string[] args)
        {
            



            Console.WriteLine("enne paluti veel mõni asi üle korrata");
            Console.WriteLine(new Inimene { Vanus = 64, Nimi = "Henn" });
            Console.WriteLine(new Inimene { Vanus = 32, Nimi = "Ants" });
            Console.WriteLine(new Inimene { Vanus = 16, Nimi = "Peeter" });
            Console.WriteLine(new Inimene { Vanus = 8, Nimi = "Kalle" });
            Inimene.Palju();

            Inimene malle = new Inimene { Nimi = "Malle", Vanus = 22 };
            Console.WriteLine(malle.Nr);

            Loom1 l1 = new Loom1();
            l1.Nimi = "Muri";
            Loom2 l2; // = new Loom2(); - strukti EI PEA LOOMA
            l2.Nimi = "Pontu";
            l2.Tõug = "krants";

        }



    }

    class Loom1   // class on oluliselt paindlikum
    {
        public string Nimi;
        public string Tõug;
    }
    struct Loom2   // natuke odavam pidada
    {
        public string Nimi;
        public string Tõug;
    }
    // NB! Kõigis keeltes EI PRUUGI kahte erinevat asja olla

    class Inimene
    {
        public static int InimesteArv { get; private set; } = 0;

        public static void Palju()
        {
            Console.WriteLine("inimesi on meil {0}", InimesteArv);
        }

        // public readonly int Nr = ++InimesteArv;
        public int Nr { get; } = ++InimesteArv;
        
        public string Nimi { get; set; }
        public int Vanus { get; set; }

        public override string ToString()
        {
            return $"{Nr}. (of {InimesteArv}) {Nimi} vanusega {Vanus}";
        }
    }
}